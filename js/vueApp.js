/**
 * Created by Vicnent on 21.02.2018.
 */

/**
 * Created by Vincent on 24-Nov-17.
 * TODO remove active & dropdown properties
 */

var searchApp = new Vue({
    el: '#youtubeApp',
    data: {
        historyData: [],
        videoData: [],
        position: 0,
        isLoading: false,
        processingFile: false
    },
    methods: {
        init: function() {
            var that = this;
            document.getElementById('files').addEventListener('change', this.getData, false);

            $(window).scroll(function() {
                if($(window).scrollTop() + $(window).height() > $(document).height() - 750) {
                    that.search();
                }
            });
        },
        search: function () {
                var that = this;
                if (this.historyData.length !== 0) {
                    if (!that.isLoading) {
                        that.isLoading = true;
                        axios.post('/getVideos/', {historyData: that.historyData.slice(that.position, (that.position + 50))})
                            .then(function (response) {
                                that.videoData = that.videoData.concat(response.data.results);
                                that.position += 50;
                                that.isLoading = false;
                            });
                    }
                } else {
                    // console.log("load file first");
                }
        },
        getData: function(evt) {
            var files = evt.target.files; // FileList object
            var that = this;
            var reader = new FileReader();
            this.processingFile = true;

            reader.onload = function(e) {
                var text = reader.result;
                var htmlData = jQuery.parseHTML(text);

                $(htmlData[2]).find(".outer-cell").each(function() {
                    let id = $(this).find("a").html().replace("https://www.youtube.com/watch?v=", "");
                    let dateTime = $(this).find(".content-cell").find('br').get(0).nextSibling.nodeValue;
                    that.historyData.push({
                        id: id,
                        date: dateTime
                    });
                });
                that.historyData.reverse();
                that.search();
                that.processingFile = false;
            };

            reader.readAsText(files[0], "UTF-8");
        }
    },
    mounted: function () {
        this.init();

        this.$nextTick(function () {
            // Code that will run only after the
            // entire view has been rendered
        })
    }
});