/**
 * Created by Vicnent on 21.02.2018.
 */
var express = require('express');
var router = express.Router();
var request = require('request');
var config = require('../config/config');

router.get('/', function (req, res, next) {
    res.render('index');
});

router.post('/getVideos', function (req, response, next) {
    let historyData = req.body.historyData;
    let idArr = [];

    historyData.forEach(function(element) {
        idArr.push(element.id);
    });

    let videoQuery = idArr.join(",");

    request.get({
        url: `https://www.googleapis.com/youtube/v3/videos?part=id%2C+snippet&id=${videoQuery}&key=${config.API_KEY}`,
        json: true,
        headers: {'User-Agent': 'request'}
    }, (err, res, data) => {
        if (err) {
            console.log('Error:', err);
        } else if (res.statusCode !== 200) {
            console.log('Status:', res.statusCode);
        } else {
            let results = [];

            data.items.forEach(function(entry) {
                let dataEntry = historyData.filter(
                    function(data){ return data.id == entry.id }
                );

                let description = entry.snippet.description;
                if (description.length > 150) {
                    description = `${description.substring(0,150)}...`;
                }

                let result = {
                    id: entry.id,
                    href: "https://www.youtube.com/watch?v=" + entry.id,
                    title: entry.snippet.title,
                    description: description,
                    date: dataEntry[0].date,
                    image: entry.snippet.thumbnails.medium.url
                };

                results.push(result);
            });

            console.log(results);
            response.send({results: results});
        }
    });
});

module.exports = router;