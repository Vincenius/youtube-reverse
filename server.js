var express = require('express');

var app = express();
var http = require('http').Server(app);
var url = require("url");
var fs = require('fs');
var bodyParser = require('body-parser');
var index = require('./routes/index');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname));
app.use('/', index);

app.get('/', function (req, res) {
    var pathname = url.parse(req.url).pathname;
    res.render('index');
});

//_____Server auf Port starten_____/
http.listen(3006, function () {
    console.log('listening on *:3006');
});

